## Homework

# Data

* budget_per_min -> 17
* lux_per_min -> 40
* fixed_per_km -> 13
* allowed_devat -> 17
* inno_disc -> 11

# Table

| Conditions(inputs) | Values                            | R1       | R2       | R3  | R4  | R5  | R6  | R7       | R8          | R9          | R10         | R11         | R12    | R13    |
|--------------------|-----------------------------------|----------|----------|-----|-----|-----|-----|----------|-------------|-------------|-------------|-------------|--------|--------|
| Type               | "budget","luxury", nonsense       | nonsense | *        | *   | *   | *   | *   | *        | budget      | budget      | luxury      | luxury      | *      | *      |
| Plan               | "minute", "fixed_price", nonsense | *        | nonsense | *   | *   | *   | *   | *        | fixed_price | fixed_price | fixed_price | fixed_price | minute | minute |
| Distance           | >0, <=0                           | *        | *        | <=0 | *   | *   | *   | *        | >0          | >0          | >0          | >0          | >0     | >0     |
| PlannedDistance    | >0, <=0                           | *        | *        | *   | <=0 | *   | *   | *        | >0          | >0          | >0          | >0          | >0     | >0     |
| Time               | >0, <=0                           | *        | *        | *   | *   | <=0 | *   | *        | >0          | >0          | >0          | >0          | >0     | >0     |
| PlannedTime        | >0, <=0                           | *        | *        | *   | *   | *   | <=0 | *        | >0          | >0          | >0          | >0          | >0     | >0     |
| InnoDiscount       | "yes", "no", nonsense             | *        | *        | *   | *   | *   | *   | nonsense | "yes"       | "no"        | "yes"       | "no"        | "yes"  | "no"   |
|                    |                                   |          |          |     |     |     |     |          |             |             |             |             |        |        |
| Invalid Request    |                                   | X        | X        | X   | X   | X   | X   | X        |             |             | X           | X           |        |        |
| 200                |                                   |          |          |     |     |     |     |          | X           | X           |             |             | X      | X      |

# Tests

| Type | Plan | Distance | Planned Distance | Time | Planned Time | Discount | Expected Result | Result
|------|------|----------|------------------|------|--------------|----------| --------------- | ------ |
| budget | minute | 1 | 1 | 1 | 1 | yes | 15.129999999999999 | {"price":15.13} |
| budget | minute | 1 | 1 | 1 | 1 | no | 17 | {"price":17} |
| budget | minute | -1 | 1 | 1 | 1 | yes | Invalid Request | {"price":15.13} |
| budget | minute | 1 | -1 | 1 | 1 | yes | Invalid Request | {"price":15.13} |
| budget | minute | 1 | 1 | -1 | 1 | yes | Invalid Request | Invalid Request |
| budget | minute | 1 | 1 | 1 | -1 | yes | Invalid Request | Invalid Request |
| luxury | minute | 1 | 1 | 1 | 1 | yes | 35.6 | {"price":35.6} |
| luxury | minute | 1 | 1 | 1 | 1 | no | 40 | {"price":40} |
| luxury | fixed_price | 1 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |
| budget | fixed_price | 1 | 1 | 1 | 1 | yes | 11.57 | {"price":11.125} |
| budget | fixed_price | 1 | 1 | 1 | 1 | no | 13 | {"price":12.5} |
| budget | fixed_price | 10 | 1 | 1 | 1 | no | 13 | {"price":16.666666666666668} |
| budget | fixed_price | 1 | 1 | 10 | 1 | no | 166 | {"price":166.66666666666666} |
| budget | fixed_price | 1 | 1 | 1 | 1 | no | 13 | {"price":12.5} |
| luxury | minute | 1 | 10 | 1 | 1 | yes | 35.6 | {"price":35.6} |
| luxury | minute | 1 | 1 | 1 | 10 | yes | 35.6 | {"price":35.6} |
| luxury | minute | 10 | 1 | 1 | 1 | yes | 35.6 | {"price":35.6} |
| luxury | minute | 1 | 1 | 10 | 1 | yes | 356.0 | {"price":356} |
| luxury | minute | 0 | 1 | 1 | 1 | yes | Invalid Request | {"price":35.6} |
| luxury | minute | 1 | 0 | 1 | 1 | yes | Invalid Request | {"price":35.6} |
| luxury | minute | 1 | 1 | 0 | 1 | yes | Invalid Request | {"price":0} |
| luxury | minute | 1 | 1 | 1 | 0 | yes | Invalid Request | {"price":35.6} |
| luxury | minute | 10 | 0 | 1 | 1 | yes | Invalid Request | {"price":35.6} |
| luxury | minute | 1 | 1 | 10 | 0 | yes | Invalid Request | {"price":356} |
| luxury | minute | 1 | 10 | 1 | 10 | yes | 35.6 | {"price":35.6} |
| nonsense | minute | 1 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| luxury | nonsense | 1 | 1 | 1 | 1 | yes | 35.6 | Invalid Request |
| luxury | minute | 1 | 1 | 1 | 1 | nonsense | Invalid Request | {"price":40} |
| luxury | minute | -1 | 1 | 1 | 1 | yes | Invalid Request | {"price":35.6} |
| luxury | minute | 1 | -1 | 1 | 1 | yes | Invalid Request | {"price":35.6} |
| luxury | minute | 1 | 1 | -1 | 1 | yes | Invalid Request | Invalid Request |
| luxury | minute | 1 | 1 | 1 | -1 | yes | Invalid Request | Invalid Request |
